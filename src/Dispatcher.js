import ListenerConstructor from './ListenerConstructor';
import { is } from 'immutable';

export default class Dispatcher {
    constructor() {
        this.stores = [];
        this.globalListeners = [];
        this.dispatch = this.dispatch.bind(this); 
    }

    registerStore(store) {
        this.stores.push(store);
    }

    addGlobalListener(listener) {
        return ListenerConstructor(listener, this.globalListeners);
    }

    dispatch(action) {
        if (this.dispatching) {
            throw new Error('Already dispatching');
        }
        this.dispatching = true;
        const changedStores = [];
        this.stores.forEach((store) => {
            const initialState = store.getState();
            const reducedState = store.reduce(action, initialState);
            store.setState(reducedState);
            if (!is(initialState, reducedState)) {
                changedStores.push(store);
            }
        });
        changedStores.forEach(store => store.triggerListeners(action));
        if (changedStores.length) {
            this.globalListeners.forEach(listener => listener(action));
        }
        this.dispatching = false;
    }

}
