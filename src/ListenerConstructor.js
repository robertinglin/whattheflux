export default (listener, listeners) => {
    if (typeof listener === 'function') {
        listeners.push(listener);
        return () => {
            let index = -1;
            listeners.every((l, i) => {
                if (l === listener) {
                    index = i;
                    return false;
                }
                return true;
            });
            if (index > -1) {
                listeners.splice(index, 1);
            } 
        };
    }
    throw new Error('listener is not a function');
}
