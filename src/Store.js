import ListenerConstructor from './ListenerConstructor';
let stateManager;
let dispatcher;

export default class Store {
    constructor(storeName, defaultState) {
        this.storeName = storeName;
        this.listeners = [];
        if (!stateManager || !stateManager.registerState) {
            throw new Error('StateManager not set')
        }
        if (!dispatcher || !dispatcher.registerStore) {
            throw new Error('Dispatcher not set');
        }

        stateManager.registerState(storeName, defaultState);
        dispatcher.registerStore(this);
    }

    getState() {
        return stateManager.getState(this.storeName);
    }

    setState(state) {
        stateManager.setState(this.storeName, state);
    }

    reduce(action, state) {
        return state;
    }

    addListener(listener) {
        return ListenerConstructor(listener, this.listeners);
    }

    triggerListeners(action) {
        this.listeners.forEach(listener => listener(action));
    }
}

export const setStateManager = (_stateManager) => {
    stateManager = _stateManager;
};

export const setDispatcher = (_dispatcher) => {
    dispatcher = _dispatcher;
};