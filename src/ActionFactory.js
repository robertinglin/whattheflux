
export default (actionName, data) => {
    return {
        name: actionName,
        data: data
    };
};
