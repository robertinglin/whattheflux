import Dispatcher from './Dispatcher';
import StateManager from './StateManager';
import { setStateManager, setDispatcher } from './Store';

export default () => {
    const stateManager = new StateManager();
    const dispatcher = new Dispatcher();
    setStateManager(stateManager);
    setDispatcher(dispatcher);

    return {
        dispatch: dispatcher.dispatch
    };
};
