import Immutable, { isImmutable } from 'immutable';

export default class StateManager {
    constructor() {
        this.states = {};
        this.defaultStates = {};
    }

    registerState(storeName, initialState) {
        if (this.states[storeName]) {
            throw new Error('State already registered');
        }
        this.defaultStates[storeName] = Immutable.fromJS(initialState);
        this.states[storeName] = this.defaultStates[storeName];
    }

    getState(storeName) {
        if (!this.states[storeName]) {
            throw new Error('Store not registered');
        }
        return this.states[storeName]
    }

    getStateJSON(storeName) {
        return this.getState(storeName).toJS();
    }

    setState(storeName, state) {
        if (!isImmutable(state)) {
            throw new Error('State passed is not Immutable');
        }
        this.states[storeName] = state;
    }
}
