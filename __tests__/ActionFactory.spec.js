import ActionFactory from '../src/ActionFactory';

test('ActionFactory', () => {
    expect(ActionFactory('Test', { test: 'test' })).toEqual({ name: 'Test', data: { test: 'test' } });
});
