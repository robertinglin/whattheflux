import Store, { setStateManager, setDispatcher } from '../src/Store';


describe('Store', () => {
    let store = null;
    let mockStateManager;
    let dispatcher;
    beforeEach(() => {
        mockStateManager = {
            registerState: jest.fn(),
            getState: jest.fn(),
            setState: jest.fn()
        };
        dispatcher = {
            registerStore: jest.fn()
        };
        setDispatcher(dispatcher);
        setStateManager(mockStateManager);
    });

    describe('constructor', () => {
        test('registers with stateManager', () => {
            expect(() => {
                store = new Store('testStore', {a:'b'});
            }).not.toThrow();
            expect(mockStateManager.registerState.mock.calls.length).toBe(1);
        });

        test('throws if the stateManager is not defined', () => {
            setStateManager(null);
            expect(() => {
                new Store('testStore', {a:'b'});
            }).toThrow('StateManager not set');
        });

        test('throws if the dispatcher is not defined', () => {
            setDispatcher(null);
            expect(() => {
                new Store('testStore', {a:'b'});
            }).toThrow('Dispatcher not set');
        });
    });
    
    describe('getState', () => {
        test('calls stateManager', () => {
            store = new Store('testStore', {a:'b'});
            const state = store.getState();
            expect(mockStateManager.getState.mock.calls.length).toBe(1);
        });
    });

    describe('setState', () => {
        test('calls stateManager', () => {
            store = new Store('testStore', {a:'b'});
            store.setState();
            expect(mockStateManager.setState.mock.calls.length).toBe(1);
        });
    });

    describe('reduce', () => {
        test('returns state passed in', () => {
            store = new Store('testStore', {a:'b'});
            const testObj = {a: 'b', c: 'd'}; 
            expect(store.reduce(null, testObj)).toBe(testObj);
        });
    });

    describe('listeners', () => {
        test('addListener', () => {
            store = new Store('testStore', {a:'b'});
            const listener = () => {};
            store.addListener(listener);
            expect(store.listeners[0]).toBe(listener);
        });

        test('triggerListeners', () => {
            store = new Store('testStore', {a:'b'});
            const listener = jest.fn();
            const action = {test: 'test'};
            store.addListener(listener);
            store.triggerListeners(action);
            expect(listener.mock.calls.length).toBe(1);
            expect(listener.mock.calls[0][0]).toEqual(action);
        });
    });
});
