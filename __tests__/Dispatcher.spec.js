import Dispatcher from '../src/Dispatcher';
import StateManager from '../src/StateManager';
import Store, { setStateManager, setDispatcher } from '../src/Store';

describe('Dispatcher', () => {
    let dispatcher;

    beforeEach(() => {
        dispatcher = new Dispatcher();
    });

    test('registerStore', () => {
        const test = {test: 'test'};
        dispatcher.registerStore(test);
        expect(dispatcher.stores.length).toBe(1);
        expect(dispatcher.stores[0]).toBe(test);
    });

    test('addGlobalListener', () => {
        const listener = () => {};
        dispatcher.addGlobalListener(listener);
        expect(dispatcher.globalListeners.length).toBe(1);
        expect(dispatcher.globalListeners[0]).toBe(listener);
    });

    describe('dispatch', () => {
        let store;
        beforeEach(() => {
            setStateManager(new StateManager());
            setDispatcher(dispatcher);
            store = new Store('testStore', {test: 'a'});
        });

        test('fires listeners', () => {
            store.reduce = (action, state) => {
                return state.set('test', 'b');
            };
            store.addListener(jest.fn());
            dispatcher.addGlobalListener(jest.fn());
            dispatcher.dispatch({});
            expect(store.listeners[0].mock.calls.length).toBe(1);
            expect(dispatcher.globalListeners[0].mock.calls.length).toBe(1);
        });

        test('works without any changes', () => {
            dispatcher.dispatch({});
        });

        test('throws on dispatch in dispatch', () => {
            store.reduce = (action, state) => {
                return state.set('test', 'b');
            };
            store.addListener(() => {
                expect(() => {
                    dispatcher.dispatch({});
                }).toThrow('Already dispatching');
            });
            dispatcher.dispatch({});
        });

    });
});