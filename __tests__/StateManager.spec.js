import { isImmutable, is } from 'immutable';
import StateManager from '../src/StateManager';

describe('StateManager', () => {
    let stateManager = null;

    beforeEach(() => {
        stateManager = new StateManager();
    });

    describe('registerState', () => {
        test('adds an immutable state object', () => {
            stateManager.registerState('testState', {test: 'state'});
            const testState = stateManager.states['testState'];
            expect(testState).not.toBeUndefined();
            expect((isImmutable(testState)))
        });

        test('fails to register a state twice', () => {
            stateManager.registerState('testState', {test: 'state'});
            expect(() => {
                stateManager.registerState('testState', {test: 'state'});
            }).toThrow('State already registered');
        });
    });

    describe('getState', () => {
        test('retrieves an immutable state object', () => {
            stateManager.registerState('testState', {test: 'state'});
            const testState = stateManager.getState('testState');
            expect(testState).not.toBeUndefined();
            expect((isImmutable(testState)))
        });

        test('throws if store requested is not defined', () => {
            expect(() => {
                stateManager.getState('unregisteredState');
            }).toThrow('Store not registered');
        });
    });

    test('getStateJSON', () => {
        stateManager.registerState('testState', {test: 'state'});
        expect(stateManager.getStateJSON('testState')).toEqual({test: 'state'});
    });

    describe('setState', () => {
        beforeEach(() => {
            stateManager.registerState('testState', {test: 'state'});
        });

        test('updates state', () => {
            const testState = stateManager.getState('testState');
            const updatedState = testState.set('test', 'updated');
            stateManager.setState('testState', updatedState);
            expect(is(stateManager.getState('testState'), testState)).toBe(false);
        });

        test('mutable state fails to set', () => {
            expect(() => {
                stateManager.setState('testState', {test:'failed'});
            }).toThrow('State passed is not Immutable');
        });
    });
});
