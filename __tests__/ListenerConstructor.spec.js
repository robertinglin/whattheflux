import ListenerConstructor from '../src/ListenerConstructor';

describe('ListenerConstructor', () => {
    test('adds listener to array', () => {
        const listeners = [];
        const listener = () => {};
        ListenerConstructor(listener, listeners);
        expect(listeners[0]).toBe(listener);
    });

    test('returns a deregister', () => {
        const listeners = [];
        const listener = () => {};
        const listener2 = () => {};
        ListenerConstructor(listener2, listeners);
        const deregister = ListenerConstructor(listener, listeners);
        deregister();
        expect(listeners.length).toBe(1);
        expect(listeners[0]).toBe(listener2);
    });

    test('deregister doesn\'t fail if called twice', () => {
        const listeners = [];
        const listener = () => {};
        const deregister = ListenerConstructor(listener, listeners);
        deregister();
        expect(deregister).not.toThrow();
    });

    test('throws if listener is not a function', () => {
        expect(() => {
            ListenerConstructor({}, [])
        }).toThrow('listener is not a function')
    });
});